<!-- Please update value in the {}  -->

<h1 align="center">Checkout Page Master </h1>

<div align="center">
  <h3>
    <a href="https://{[your-demo-link.your-domain](https://pagecarmaster.vercel.app/)}">
      Demo
    </a>
    <span> | </span>
    <a href="https://{your-url-to-the-solution}">
      Solution
    </a>
    <span> | </span>
    <a href="https://devchallenges.io/challenges/0J1NxxGhOUYVqihwegfO">
      Website Projects
    </a>
  </h3>
</div>

<!-- TABLE OF CONTENTS -->

## Table of Contents

- [Table of Contents](#table-of-contents)
- [Overview](#overview)
  - [Built With](#built-with)
- [Contact](#contact)


<!-- OVERVIEW -->

## Overview

![screenshot](https://emprendeyourlifestyle.com/wp-content/uploads/2022/02/checkout.png)

This project is a simple design with a form and shopping car. I allow me to learn more about design web responsive, handling of forms and semantic tags.

### Built With

<!-- This section should list any major frameworks that you built your project using. Here are a few examples.-->

- [HTML](https://developer.mozilla.org/en-US/docs/Web/HTML)
- [CSS](https://developer.mozilla.org/en-US/docs/Web/HTML)

## Contact

- Website [your-website.com](https://{your-web-site-link})
- GitHub [@your-username](https://{github.com/your-usermame})
- Twitter [@your-twitter](https://{twitter.com/your-username})


<span class="material-icons-outlined">

</span>